package com.mectrics.metricsAPI

import com.mectrics.metricsAPI.controller.MetricController
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

// These are more like integration tests, which seemed more appropriate with the exercise.

@WebMvcTest(MetricController::class)
internal class MetricsApiApplicationTests {

	@Autowired
	private lateinit var mockMvc: MockMvc

	@Test
	fun `add metric success`() {
		mockMvc.put("/metrics/create/testMetric1") {
			contentType = MediaType.TEXT_PLAIN
		}.andExpect {
			status { isOk }
			content { contentType(MediaType.APPLICATION_JSON)}
			content { json("{'error': null, 'message': 'Metric added!', 'value':null}") }
		}
	}

	@Test
	fun `add metric exists`() {
		mockMvc.put("/metrics/create/testMetric2") {
			contentType = MediaType.TEXT_PLAIN
		}
		mockMvc.put("/metrics/create/testMetric2") {
			contentType = MediaType.TEXT_PLAIN
		}.andExpect {
			status { isBadRequest }
			content { contentType(MediaType.APPLICATION_JSON)}
			content { json("{'error':'testMetric2 metric already exists!', 'message': null, 'value':null}") }
		}
	}

	@Test
	fun `add metric value success`() {
		mockMvc.put("/metrics/create/testMetric3") {
			contentType = MediaType.TEXT_PLAIN
		}
		mockMvc.post("/metrics/addValue/testMetric3/42.1") {
			contentType = MediaType.TEXT_PLAIN
		}.andExpect {
			status { isOk }
			content { contentType(MediaType.APPLICATION_JSON)}
			content { json("{'error':null, 'message': 'Value added!', 'value':null}") }
		}
	}

	@Test
	fun `add metric value fail, metric does not exist`() {
		mockMvc.post("/metrics/addValue/testMetric4/42.1") {
			contentType = MediaType.TEXT_PLAIN
		}.andExpect {
			status { isBadRequest }
			content { contentType(MediaType.APPLICATION_JSON)}
			content { json("{'error': 'testMetric4 metric does not exist!', 'message': null, 'value':null}") }
		}
	}

	@Test
	fun `add invalid value for metric`() {
		mockMvc.put("/metrics/create/testMetricInvalidValue") {
			contentType = MediaType.TEXT_PLAIN
		}
		mockMvc.post("/metrics/addValue/testMetricInvalidValue/err") {
			contentType = MediaType.TEXT_PLAIN
		}.andExpect {
			status { isBadRequest }
		}
	}

	@Test
	fun `get mean of metric`() {
		mockMvc.put("/metrics/create/testMetric4") {
			contentType = MediaType.TEXT_PLAIN
		}
		mockMvc.post("/metrics/addValue/testMetric4/42") { contentType = MediaType.TEXT_PLAIN }
		mockMvc.post("/metrics/addValue/testMetric4/20") { contentType = MediaType.TEXT_PLAIN }
		mockMvc.post("/metrics/addValue/testMetric4/100") { contentType = MediaType.TEXT_PLAIN }

		mockMvc.get("/metrics/getMean/testMetric4") {
			contentType = MediaType.TEXT_PLAIN
		}.andExpect {
			status { isOk }
			content { contentType(MediaType.APPLICATION_JSON)}
			content { json("{'error':null, 'message': null, 'value':54.0}") }
		}
	}

	@Test
	fun `get median of metric with odd number of values`() {
		mockMvc.put("/metrics/create/testMetric5") {
			contentType = MediaType.TEXT_PLAIN
		}
		mockMvc.post("/metrics/addValue/testMetric5/42") { contentType = MediaType.TEXT_PLAIN }
		mockMvc.post("/metrics/addValue/testMetric5/20") { contentType = MediaType.TEXT_PLAIN }
		mockMvc.post("/metrics/addValue/testMetric5/100") { contentType = MediaType.TEXT_PLAIN }

		mockMvc.get("/metrics/getMedian/testMetric5") {
			contentType = MediaType.TEXT_PLAIN
		}.andExpect {
			status { isOk }
			content { contentType(MediaType.APPLICATION_JSON)}
			content { json("{'error':null, 'message': null, 'value':42.0}") }
		}
	}

	@Test
	fun `get median of metric with even number of values`() {
		mockMvc.put("/metrics/create/testMetric6") {
			contentType = MediaType.TEXT_PLAIN
		}
		mockMvc.post("/metrics/addValue/testMetric6/42") { contentType = MediaType.TEXT_PLAIN }
		mockMvc.post("/metrics/addValue/testMetric6/20") { contentType = MediaType.TEXT_PLAIN }
		mockMvc.post("/metrics/addValue/testMetric6/100") { contentType = MediaType.TEXT_PLAIN }
		mockMvc.post("/metrics/addValue/testMetric6/58") { contentType = MediaType.TEXT_PLAIN }

		mockMvc.get("/metrics/getMedian/testMetric6") {
			contentType = MediaType.TEXT_PLAIN
		}.andExpect {
			status { isOk }
			content { contentType(MediaType.APPLICATION_JSON)}
			content { json("{'error':null, 'message': null, 'value':50.0}") }
		}
	}

	@Test
	fun `get max of metric`() {
		mockMvc.put("/metrics/create/testMetric7") {
			contentType = MediaType.TEXT_PLAIN
		}
		mockMvc.post("/metrics/addValue/testMetric7/42") { contentType = MediaType.TEXT_PLAIN }
		mockMvc.post("/metrics/addValue/testMetric7/20") { contentType = MediaType.TEXT_PLAIN }
		mockMvc.post("/metrics/addValue/testMetric7/100") { contentType = MediaType.TEXT_PLAIN }

		mockMvc.get("/metrics/getMax/testMetric7") {
			contentType = MediaType.TEXT_PLAIN
		}.andExpect {
			status { isOk }
			content { contentType(MediaType.APPLICATION_JSON)}
			content { json("{'error':null, 'message': null, 'value':100.0}") }
		}
	}

	@Test
	fun `get min of metric`() {
		mockMvc.put("/metrics/create/testMetric8") {
			contentType = MediaType.TEXT_PLAIN
		}

		mockMvc.post("/metrics/addValue/testMetric8/42") { contentType = MediaType.TEXT_PLAIN }
		mockMvc.post("/metrics/addValue/testMetric8/20") { contentType = MediaType.TEXT_PLAIN }
		mockMvc.post("/metrics/addValue/testMetric8/100") { contentType = MediaType.TEXT_PLAIN }

		mockMvc.get("/metrics/getMin/testMetric8") {
			contentType = MediaType.TEXT_PLAIN
		}.andExpect {
			status { isOk }
			content { contentType(MediaType.APPLICATION_JSON)}
			content { json("{'error':null, 'message': null, 'value':20.0}") }
		}
	}

	@Test
	fun `get min of non existing metric`() {
		mockMvc.get("/metrics/getMin/testMetric9") {
			contentType = MediaType.TEXT_PLAIN
		}.andExpect {
			status { isBadRequest }
			content { contentType(MediaType.APPLICATION_JSON)}
			content { json("{'error': 'testMetric9 metric does not exist!', 'message': null, 'value': null}") }
		}
	}

	@Test
	fun `get max of non existing metric`() {
		mockMvc.get("/metrics/getMax/testMetric9") {
			contentType = MediaType.TEXT_PLAIN
		}.andExpect {
			status { isBadRequest }
			content { contentType(MediaType.APPLICATION_JSON)}
			content { json("{'error': 'testMetric9 metric does not exist!', 'message': null, 'value': null}") }
		}
	}

	@Test
	fun `get median of non existing metric`() {
		mockMvc.get("/metrics/getMedian/testMetric9") {
			contentType = MediaType.TEXT_PLAIN
		}.andExpect {
			status { isBadRequest }
			content { contentType(MediaType.APPLICATION_JSON)}
			content { json("{'error': 'testMetric9 metric does not exist!', 'message': null, 'value': null}") }
		}
	}

	@Test
	fun `get mean of non existing metric`() {
		mockMvc.get("/metrics/getMean/testMetric9") {
			contentType = MediaType.TEXT_PLAIN
		}.andExpect {
			status { isBadRequest }
			content { contentType(MediaType.APPLICATION_JSON)}
			content { json("{'error': 'testMetric9 metric does not exist!', 'message': null, 'value': null}") }
		}
	}

	@Test
	fun `get mean of metric with no values`() {
		mockMvc.put("/metrics/create/testMetric10") {
			contentType = MediaType.TEXT_PLAIN
		}
		mockMvc.get("/metrics/getMean/testMetric10") {
			contentType = MediaType.TEXT_PLAIN
		}.andExpect {
			status { isBadRequest }
			content { contentType(MediaType.APPLICATION_JSON)}
			content { json("{'error': 'testMetric10 metric does not contain any values!', 'message': null, 'value': null}") }
		}
	}

	@Test
	fun `get median of metric with no values`() {
		mockMvc.put("/metrics/create/testMetric11") {
			contentType = MediaType.TEXT_PLAIN
		}
		mockMvc.get("/metrics/getMean/testMetric11") {
			contentType = MediaType.TEXT_PLAIN
		}.andExpect {
			status { isBadRequest }
			content { contentType(MediaType.APPLICATION_JSON)}
			content { json("{'error': 'testMetric11 metric does not contain any values!', 'message': null, 'value': null}") }
		}
	}

	@Test
	fun `get min of metric with no values`() {
		mockMvc.put("/metrics/create/testMetric12") {
			contentType = MediaType.TEXT_PLAIN
		}
		mockMvc.get("/metrics/getMean/testMetric12") {
			contentType = MediaType.TEXT_PLAIN
		}.andExpect {
			status { isBadRequest }
			content { contentType(MediaType.APPLICATION_JSON)}
			content { json("{'error': 'testMetric12 metric does not contain any values!', 'message': null, 'value': null}") }
		}
	}

	@Test
	fun `get max of metric with no values`() {
		mockMvc.put("/metrics/create/testMetric13") {
			contentType = MediaType.TEXT_PLAIN
		}
		mockMvc.get("/metrics/getMean/testMetric13") {
			contentType = MediaType.TEXT_PLAIN
		}.andExpect {
			status { isBadRequest }
			content { contentType(MediaType.APPLICATION_JSON)}
			content { json("{'error': 'testMetric13 metric does not contain any values!', 'message': null, 'value': null}") }
		}
	}
}
