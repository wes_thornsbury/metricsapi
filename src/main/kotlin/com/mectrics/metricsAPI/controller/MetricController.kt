package com.mectrics.metricsAPI.controller

import com.mectrics.metricsAPI.exceptions.MetricDoesNotExistException
import com.mectrics.metricsAPI.exceptions.MetricExistsException
import com.mectrics.metricsAPI.model.Metrics.addMetric
import com.mectrics.metricsAPI.model.Metrics.addValue
import com.mectrics.metricsAPI.model.Metrics.getMax
import com.mectrics.metricsAPI.model.Metrics.getMean
import com.mectrics.metricsAPI.model.Metrics.getMedian
import com.mectrics.metricsAPI.model.Metrics.getMin
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/metrics")
class MetricController {

    @RequestMapping(value = ["/create/{metricName}"], method = [(RequestMethod.PUT)])
    fun createMetric(@PathVariable metricName: String): ResponseEntity<Any> {
        var message = "Metric added!"
        try {
            addMetric(metricName)
        }
        catch (e: MetricExistsException) {
            return ResponseEntity.badRequest().body(ResponseJson(error = e.message))
        }
        catch (e: Exception) {
            return ResponseEntity.badRequest().body(ResponseJson(error = e))  // NOT FOR PROD
        }
        return ResponseEntity.ok().body(ResponseJson(message = message))
    }

    @RequestMapping(value = ["/addValue/{metricName}/{value}"], method = [(RequestMethod.POST)])
    fun addMetricValue(@PathVariable metricName: String, @PathVariable value: Double): ResponseEntity<Any> {
        var message = "Value added!"
        try {
            addValue(metricName, value)
        }
        catch (e: MetricDoesNotExistException){
            return ResponseEntity.badRequest().body(ResponseJson(error = e.message))
        }
        catch (e: Exception) {
            return ResponseEntity.badRequest().body(ResponseJson(error = e))  // NOT FOR PROD
        }
        return ResponseEntity.ok().body(ResponseJson(message = message))
    }

    @RequestMapping(value = ["/getMean/{metricName}"], method = [(RequestMethod.GET)])
    fun getMetricMean(@PathVariable metricName: String): ResponseEntity<Any> {
        var mean: Double
        try {
            mean = getMean(metricName)
        }
        catch (e: Exception){
            return ResponseEntity.badRequest().body(ResponseJson(error = e.message))  // NOT FOR PROD
        }
        return ResponseEntity.ok().body(ResponseJson(value = mean))
    }

    @RequestMapping(value = ["/getMedian/{metricName}"], method = [(RequestMethod.GET)])
    fun getMetricMedian(@PathVariable metricName: String): ResponseEntity<Any> {
        var median: Double
        try {
            median = getMedian(metricName)
        }
        catch (e: Exception){
            return ResponseEntity.badRequest().body(ResponseJson(error = e.message))
        }
        return ResponseEntity.ok().body(ResponseJson(value = median))
    }

    @RequestMapping(value = ["/getMin/{metricName}"], method = [(RequestMethod.GET)])
    fun getMetricMin(@PathVariable metricName: String): ResponseEntity<Any> {
        var min: Double
        try {
            min = getMin(metricName)
        }
        catch (e: Exception){
            return ResponseEntity.badRequest().body(ResponseJson(error = e.message))
        }
        return ResponseEntity.ok().body(ResponseJson(value = min))
    }

    @RequestMapping(value = ["/getMax/{metricName}"], method = [(RequestMethod.GET)])
    fun getMetricMax(@PathVariable metricName: String): ResponseEntity<Any> {
        var max: Double
        try {
            max = getMax(metricName)
        }
        catch (e: Exception){
            return ResponseEntity.badRequest().body(ResponseJson(error = e.message))
        }
        return ResponseEntity.ok().body(ResponseJson(value = max))
    }

    data class ResponseJson(
            val error: Any? = null,
            val message: String? = null,
            val value: Double? = null
    )
}