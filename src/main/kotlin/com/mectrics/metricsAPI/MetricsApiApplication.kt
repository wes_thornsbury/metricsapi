package com.mectrics.metricsAPI

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MetricsApiApplication

fun main(args: Array<String>) {
	runApplication<MetricsApiApplication>(*args)
}
