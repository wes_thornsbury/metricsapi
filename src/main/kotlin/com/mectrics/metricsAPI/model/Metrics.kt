package com.mectrics.metricsAPI.model

import com.mectrics.metricsAPI.exceptions.MetricDoesNotExistException
import com.mectrics.metricsAPI.exceptions.MetricExistsException
import com.mectrics.metricsAPI.exceptions.NoValuesForMetricException

object Metrics {
    private val metrics: HashMap<String, MutableList<Double>> = HashMap<String, MutableList<Double>>()
    // With a mutableList, I assume that adding to it is actually an amortized O(1) because the list has to 
    // be expanded every so often to make way for new items.

    // O(1) time and space Checking if a hash exists and adding it if it does not.
    fun addMetric(metricName: String) {
        if (!metrics.containsKey(metricName)) {
            metrics[metricName] = mutableListOf<Double>()
        }
        else {
            throw MetricExistsException(metricName)
        }
    }

    // O(1) time and space checking if hash exists and adding value to existing list.
    fun addValue(metricName: String, value: Double){
        if (metrics.containsKey(metricName)) {
            metrics[metricName]?.let { it.add(value) }
        }
        else {
            throw MetricDoesNotExistException(metricName)
        }
    }

    // O(n) time because the entire collection needs to be traversed in order to get each value for calculation
    // O(1) space. Data stays in place during calculations.
    fun getMean(metricName: String): Double{
        val total = metrics[metricName]?.sum() ?: throw MetricDoesNotExistException(metricName)
        val length = metrics[metricName]!!.size
        if (length == 0) throw NoValuesForMetricException(metricName)

        return total.div(length)
    }

    // O(n log n) time assuming the algorithm used by the JVM / Kotlin is a standard Merge Sort or Heap Sort.
    // O(n) space because the entire list is sorted and placed within a new variable.
    fun getMedian(metricName: String): Double{
        val length = metrics[metricName]?.size ?: throw MetricDoesNotExistException(metricName)
        if (length == 0) throw NoValuesForMetricException(metricName)

        val doublesList = metrics[metricName]!!.sorted()
        return if (length % 2 == 0) {
            (doublesList[(length-1)/2] + doublesList[length/2]) /2.0
        }
        else {
            doublesList[length/2]
        }
    }

    // O(n) time: every element has to be examined in order to tell which is the min.
    // Unless the JVM keeps meta data on the collection and updates the value on insert? O(1) time if that is the case.
    // O(1) space: Done with data in place.
    fun getMin(metricName: String): Double{
        val length = metrics[metricName]?.size ?: throw MetricDoesNotExistException(metricName)
        if (length == 0) throw NoValuesForMetricException(metricName)
        return metrics[metricName]!!.min()!!
    }

    // O(n) time: every element has to be examined in order to tell which is the max.
    // Unless the JVM keeps meta data on the collection and updates the value on insert? O(1) time if that is the case.
    // O(1) space: Done with data in place.
    fun getMax(metricName: String): Double {
        val length = metrics[metricName]?.size ?: throw MetricDoesNotExistException(metricName)
        if (length == 0) throw NoValuesForMetricException(metricName)
        return metrics[metricName]!!.max()!!
    }
}
