package com.mectrics.metricsAPI.exceptions

import java.lang.Exception

class NoValuesForMetricException(message: String): Exception(message) {
    override val message: String?
        get() = "${super.message} metric does not contain any values!"
}