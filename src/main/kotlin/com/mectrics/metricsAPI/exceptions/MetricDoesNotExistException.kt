package com.mectrics.metricsAPI.exceptions

import java.lang.Exception

class MetricDoesNotExistException(message: String): Exception(message) {
    override val message: String?
        get() = "${super.message} metric does not exist!"
}