package com.mectrics.metricsAPI.exceptions

import java.lang.Exception

class MetricExistsException(message: String): Exception(message) {
    override val message: String?
        get() = "${super.message} metric already exists!"
}