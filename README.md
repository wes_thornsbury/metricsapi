# README #

This is the MetricsAPI used as a code sample. This API is used to store metrics and retreive metrics about the data stored.
Time and space complexities are inside the Metrics model object as comments.

### RestAPI calls ###

* PUT /create/{metricName} - Add metricName to stored metrics.
* POST /addValue/{metricName}/{value} - Add decimal number value to metricName.
* GET /getMean/{MetricName} - Get mean of values in specified metric.
* GET /getMedian/{MetricName} - Get median of values in specified metric. 
* GET /getMin/{MetricName} - Get smallest number from list of values in specified metric.
* GET /getMax/{MetricName} - Get largest number from list of values in specified metric.

### How do I get set up? ###

#### Dependencies ####
Kotlin latest
JDK 11+

#### Data persistance ####
There is no data persistance beyond the runtime of the application.
Data is stored in a singleton for simlicity.

#### How to run tests ####
In the command line, navigate to the top level folder and type ./gradlew test .
An output of the test summary is output to /build/reports/tests/test/index.html .

#### Instructions for running ####
To run this sample, navigate to the top level folder in the command line and type ./gradlew bootRun .
To exit the running application, press Ctrl + C . Press Y to confirm exit.